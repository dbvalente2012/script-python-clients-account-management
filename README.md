# Script - Python - Clients Management

Python Programming - Script Code

The course final project, using Python, consisted of a program to manage clients. In this program it is possible to create clients and add personal information. There are several options for the manager to control the clients data: preview the client information, search for a client using the identification number, modify costumer information and delete a client in the database using a unique identifier. There is also the possibility of exporting and importing the clients information through CSV, JSON  and MySQL data files.  

Important:  -> MySql database only work with xampp. Currently dont have online database.
-> Before working with files, you should check the file directory. Choose the desired directory.

Developer : Duarte Borges Valente

June to September 2020

SPyder  - Python 3.7.2
