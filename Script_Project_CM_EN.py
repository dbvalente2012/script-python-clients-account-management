from datetime import date, time, datetime
array=[]
array_csv=[]


import os.path #Biblioteca que vai ser usada na função controlo_ficheiro_json() que verifica se o documento está criado ou não
import json
import csv
import os
diretoria_json='D:\\json.json'
diretoria_csv='D:\\csv.csv'

def menu():
    print("\n****Clients Management ****\n")
    print('\n1)  Add Clients \n2)  View Client by criation order \n3)  View Client by name\n4)  Find Client \n5)  Delete Client \n6)  Change client information \n7)  CSV File control \n8)  Json File control\n9)  MySQL File control\n10)  Change Files directories\n11)  Close Program ')
    input_selecao=input('\nChoose your option: ')
    return input_selecao

def navegar_para_menu():    
    try:
        selecao=menu()
    except KeyboardInterrupt:
        print ('Ctrl+C is not available')
        programa_duarte()
    else:
        os.system('cls')
        if (selecao=='1'):
            introducao_clientes() #nao tem grupo
        if (selecao=='2'):
            visualizar_todos_clientes_ordem_criação() 
        if (selecao=='3'):
            visualizar_todos_clientes_ordem_nome()
        if (selecao=='4'):
            contribuinte_a_procurar()
        if (selecao=='5'):
            cliente_a_eliminar()
        if (selecao=='6'):
            cliente_a_alterar()
        if (selecao=='7'):
            controlo_ficheiro_csv() #nao tem no grupo
        if (selecao=='8'):
            controlo_ficheiro_json()
        if (selecao=='9'):
            controlo_ficheiro_MySQL()
        if (selecao=='10'):
            alterar_diretorios_ficheiros()
        if (selecao=='11'):
            sair()
        else:
            print("\n*****Repeat your option ***********")  
  

def introducao_clientes():

    global array
    global array_csv
    
    try:
        quantidade=int(input('How much clients want to add? '))
    except ValueError:
        print('Error, please write an integer number!')
        introducao_clientes()
    else:    
        contador_quantidade=0
        for contador_quantidade in range (quantidade):          
            today = date.today()
            now = datetime.now()
            current_time = time(now.hour, now.minute, now.second)              
            while True:
                try:
                    nome=input("Insert the client name: ")    
                    if nome.isdigit():
                        raise ValueError
                    else:
                        if len(nome)>0:
                            break    
                        else:
                            raise ValueError   
                except ValueError:
                    print('Error, you have to put letters in the clients name. Numbers are not accepted')    
                    
            while True:   
                try: 
                    contribuinte=int(input("Insert the client ID number card:  "))
                    if len(str(contribuinte))==9:
                        break
                    else:
                        raise ValueError     
                except ValueError:
                    print('Error, please write 9 integer numbers!')     
         
            while True:   
                try: 
                    telefone=int(input("Insert the client phonenumber:  "))
                    convert_to_string=str(telefone)
                    if len(str(telefone))==9:
                        if int(convert_to_string[0])==9 and int(convert_to_string[1])==1 or int(convert_to_string[0])==9 and int(convert_to_string[1])==2 or int(convert_to_string[0])==9 and int(convert_to_string[1])==3 or int(convert_to_string[0])==9 and int(convert_to_string[1])==6:
                            break
                        else:
                             print('You have to start the number with 91 or 92 or 93 or 96')
                    else:
                        raise ValueError     
                except ValueError:
                    print('Error, please write 9 integers for the mobile phone')   
                    
            cli={'nome':nome,'contribuinte':contribuinte,'telefone':telefone,'data':str(today),'hora':str(current_time)}
            array.append(cli)
             
            cli_csv=[nome,contribuinte,telefone,str(today),str(current_time)]
            array_csv.append(cli_csv)
            print('This is a array_csv:',array_csv)
            contador_quantidade+=1

def cliente_a_alterar():
    global array
    if len(array)!=0:
        contribuinte_a_alterar=int(input('What is the client ID number of the client you want to change?:'))
        print('\n') 
        for imprimi_arrays_indiv in array:  
            if imprimi_arrays_indiv['contribuinte']==contribuinte_a_alterar:
                print(imprimi_arrays_indiv['nome'],'\t',imprimi_arrays_indiv['contribuinte'],'\t',imprimi_arrays_indiv['telefone'],'\t',imprimi_arrays_indiv['data'],imprimi_arrays_indiv['hora'])  
                print('\nSelect your option:\n1)Change Name\n2)Change Phonenumber')
                selecao_alterar_cliente=input('\nChoose your option: ')
               
                if (selecao_alterar_cliente=='1'):
                    print('The current name:',imprimi_arrays_indiv['nome'], 'what name do you want to change now:')
                    nome_alterar=input()
                    print('\nThe name has been changed!!\n')
                    imprimi_arrays_indiv['nome']=nome_alterar
                    print('The client with the ID number',imprimi_arrays_indiv['contribuinte'], 'now has another name\n')
                    print(imprimi_arrays_indiv['nome'],'\t',imprimi_arrays_indiv['contribuinte'],'\t',imprimi_arrays_indiv['telefone'],'\t',imprimi_arrays_indiv['data'],imprimi_arrays_indiv['hora'])
               
                if (selecao_alterar_cliente=='2'):    
                    print('The phonenumber name:',imprimi_arrays_indiv['telefone'], 'what phonenumber do you want to change now:')
                    telefone_alterar=input()
                    print('\nThe phonenumber has been changed!!\n')
                    imprimi_arrays_indiv['telefone']=telefone_alterar
                    print('The client with the ID number',imprimi_arrays_indiv['contribuinte'], 'now has another phonenumber\n')
                    print(imprimi_arrays_indiv['nome'],'\t',imprimi_arrays_indiv['contribuinte'],'\t',imprimi_arrays_indiv['telefone'],'\t',imprimi_arrays_indiv['data'],imprimi_arrays_indiv['hora'])
    else:
        print('\nEmpty database, you have to add clients to start the search')
                           
def visualizar_todos_clientes_ordem_criação():
    global array
    if len(array)!=0:
        print('\n List of all clients added by order:\n')
        print('Name  |   ID number   | Phonenumber   | Creation date \n')
        print('----------------------------------------------------------')
        for numero_cliente in array:
            print(numero_cliente['nome'],'\t',numero_cliente['contribuinte'],'\t',numero_cliente['telefone'],'\t',numero_cliente['data'],numero_cliente['hora'])    
    else:
        print('\nEmpty database, you have to add clients')
        
def visualizar_todos_clientes_ordem_nome():
    global array
    if len(array)!=0:
        print('\n List of all clients added by name:\n')
        print('Name  |   ID number   | Phonenumber   | Creation date \n')
        print('--------------------------------------')
        ordem_nome = sorted(array, key=lambda k: k['nome']) # Organizar por nome nome
        for numero_cliente in ordem_nome:
            print(numero_cliente['nome'],'\t',numero_cliente['contribuinte'],'\t',numero_cliente['telefone'],'\t',numero_cliente['data'],numero_cliente['hora'])
    else:
        print('\nEmpty database, you have to add clients')
        
def contribuinte_a_procurar():
    global array
    if len(array)!=0:
        valor_contribuinte_a_procurar=int(input('What is the ID number of the client you want to look for?:'))
        print('\n')
        print('Name  |   ID number   | Phonenumber   | Creation date \n')
        print('--------------------------------------')
        for imprimi_arrays_indiv in array:  
            if imprimi_arrays_indiv['contribuinte']==valor_contribuinte_a_procurar:
                print(imprimi_arrays_indiv['nome'],'\t',imprimi_arrays_indiv['contribuinte'],'\t',imprimi_arrays_indiv['telefone'],'\t',imprimi_arrays_indiv['data'],imprimi_arrays_indiv['hora'])        
        print('\n')
    else:
        print('\nEmpty database, you have to add clients to start the search')
        
def cliente_a_eliminar():
    global array
    if len(array)!=0:
        valor_contribuinte_a_eliminar=int(input('What is the ID number of the client you want to delete?: '))
        for imprimi_arrays_indiv in array:   
                if imprimi_arrays_indiv['contribuinte']==valor_contribuinte_a_eliminar:
                        print('\nAll client information with ID number ',imprimi_arrays_indiv['contribuinte'], 'have been erased.\n')
                        array.remove(imprimi_arrays_indiv)
    else:
        print('\nEmpty database, you must have client in the database in order to delete client')
        
def sair():
    exit()
     
def programa_duarte():
    while True:  
        navegar_para_menu()            
           
def controlo_ficheiro_json():
    global array
    global diretoria_json
    print('\n1)  Save on json file \n2)  Read json file \n3)  Upload info from json file\n4)  Delete info from json file\n5)  Return menu ')
    selecao_controlo_ficheiro_json=input('\nChoose your option: ')  
    
    if (selecao_controlo_ficheiro_json=='1'):  
        if len(array)!=0:
            data = {}
            data['cliente'] = array # Cria a key cliente para cada array e guarda no dictionary data
            with open(diretoria_json, 'w') as outfile:
                json.dump(data, outfile)
            print('\n----------------------------------------------------------\nYou just saved this information in the file ',diretoria_json,'\n')
        else:
              print('\nATTENTION: The array is empty, you have to insert clients in the database before saving it to the json file')
      
            
    if (selecao_controlo_ficheiro_json=='2'):
        confirma_json_criado=os.path.exists(diretoria_json)
        if confirma_json_criado==True:
            with open(diretoria_json) as json_file:
                data = json.load(json_file)
                print(data['cliente'])  
        else:
            print('\nATTENTION: The json document you want to read is not created, you have to save information and only then can you read it.')
   
    if (selecao_controlo_ficheiro_json=='3'):  
        confirma_json_criado=os.path.exists(diretoria_json)
        if confirma_json_criado==True:
            with open(diretoria_json) as json_file:
                data = json.load(json_file)
                array = data['cliente']
                print(data)  
        else:
             print('\nATTENTION: The .json file you want to upload is not created, so you are unable to load the information. If you want to import information from .json into the program, you must first save information in the .json file.')
   
    if (selecao_controlo_ficheiro_json=='4'): 
        confirma_json_criado=os.path.exists(diretoria_json)
        if confirma_json_criado==True:        
            with open(diretoria_json, 'r+') as json_file: 
                data = json.load(json_file)
                json_file.truncate(0)   
        else:
            print('\nATTENTION: You are unable to DELETE the .json document as it is not created.') 
    
    if (selecao_controlo_ficheiro_json=='5'): 
        navegar_para_menu()   
        
def controlo_ficheiro_csv():
    global array_csv  
    print('\n1)  Save on csv file \n2)  Read csv file \n3)  Upload info from csv file\n4)  Delete info from csv file\n5)  Return menu ')
    selecao_controlo_ficheiro=input('\nChoose your option: ')
    
    if (selecao_controlo_ficheiro=='1'): 
        if len(array)!=0:
            with open(diretoria_csv, 'w',newline='') as csvfile:          
                x = csv.writer(csvfile, delimiter='\t') 
                cabecalho=['Name','ID number','Phonenumber','Date','Hour']
                x.writerow(cabecalho)
                for escreve_linha_csv in array_csv:
                    x.writerow(escreve_linha_csv)      
                print('\n----------------------------------------------------------\nYou just saved this information in the file ',diretoria_csv,'\n')   
        else:
            print('\nATTENTION: The array is empty, you have to insert clients in the database before saving it to the csv file')
    
    
    if (selecao_controlo_ficheiro=='2'):
        confirma_json_criado=os.path.exists(diretoria_csv)
        if confirma_json_criado==True:
            with open(diretoria_csv) as csvfile:
                le_array_csv = csv.reader(csvfile, delimiter='\t')
                linhas_no_csv=len(list(csv.reader(open(diretoria_csv))))
                if linhas_no_csv>=1:
                    print('-------------------------------------------------')
                    print('----Now Im looking at the .CSV database-----')
                    print('-------------------------------------------------')
                    for linha_cliente in le_array_csv:
                        for indiv_cliente in linha_cliente:
                            print(indiv_cliente,end='\t\t')      
        else:
            print('\nATTENTION: The CSV document you want to read is not created, you have to save information and only then can you read it.')
                
    if (selecao_controlo_ficheiro=='3'):
        confirma_json_criado=os.path.exists(diretoria_csv)
        if confirma_json_criado==True:       
            with open(diretoria_csv) as csvfile:
                le_array_csv = csv.reader(csvfile, delimiter='\t')
                linhas_no_csv=len(list(csv.reader(open(diretoria_csv))))
                if linhas_no_csv>=1:
                    for linha_cliente in le_array_csv:
                        array_csv.append(linha_cliente)      
                    array_csv.remove(array_csv[0])
                    print('\nATTENTION IMPORTANT: Wrote in the array but it is only possible to load information by json, in MAINTENANCE \n')
                    print(array_csv)           
        else:
             print('\nATTENTION: The .CSV document you want to upload is not created, so you are unable to load the desired information. If you want to import CSV information into the program, you must first save information in the CSV file.')
    
    
    if (selecao_controlo_ficheiro=='4'): 
        confirma_json_criado=os.path.exists(diretoria_csv)
        if confirma_json_criado==True: 
            with open(diretoria_csv, 'r+',newline='') as csvfile:          
                x = csv.writer(csvfile, delimiter='\t') 
                csvfile.truncate(0) #Apagar informação do ficheiro
            print('\nATTENTION: The .CSV document is empty, the information has been deleted at this point\n')      
        else:
            print('\nATTENTION: You cannot ERASE the CSV document as it is not created.')
            
            
            
    if (selecao_controlo_ficheiro=='5'): 
        navegar_para_menu()

def controlo_ficheiro_MySQL():
    global array   
    print('\n1)  Save on MySQL database \n2)  Read from MySQL \n3)  Delete MySQL \n4)  Return menu ')
    selecao_controlo_ficheiro_Mysql=input('\nChoose your option: ')
    print('\n')
    if (selecao_controlo_ficheiro_Mysql=='1'): 
        import pymysql
        con = pymysql.connect('localhost', 'root', '', 'proj')
        sql = 'insert into clientesduarte(nome, contribuinte, telefone) VALUES (%s,%s,%s)'
        for imprimi_arrays_indiv in array:     
            val=(imprimi_arrays_indiv['nome'],imprimi_arrays_indiv['contribuinte'],imprimi_arrays_indiv['telefone'])           
            with con:
                cur = con.cursor()
                cur.execute(sql,val)
                con.commit()
        print("You just recorded the information in the Mysql database")                 

        
    if (selecao_controlo_ficheiro_Mysql=='2'):  
        import pymysql
        def showDBClientes():
            con = pymysql.connect('localhost', 'root', '', 'proj')
            sql = 'select nome,contribuinte, telefone from clientesduarte'
            with con:
                cur = con.cursor()
                cur.execute(sql)
                rows = cur.fetchall()
                print('You are reading the Mysql information: \n')
                for row in rows:
                    print("{0} {1} {2}".format(row[0], row[1], row[2]))        
        showDBClientes()
        
    if (selecao_controlo_ficheiro_Mysql=='3'): 
        import pymysql
        id_apagar=int(input('Choose the Id Client from Mysql you want to delete: '))
        def DeleteRow():
            con = pymysql.connect('localhost', 'root', '', 'proj')      
            id=str(id_apagar)
            sql = "delete from clientesduarte where id="+id
            with con:
                cur = con.cursor()
                cur.execute(sql)
                con.commit()
                print("You just delete the client information from the Mysql database")
                    
        DeleteRow()
            
          
    
        
    if (selecao_controlo_ficheiro_Mysql=='4'):  
        navegar_para_menu() 

def alterar_diretorios_ficheiros():
    global diretoria_json
    global diretoria_csv
    print('\n*****DIRETORIAS DOS FICHEIROS DO PROGRAMA ACTUAL*****\n')
    print('Actualmente o diretorio CSV é:',diretoria_csv,'\n')
    print('Actualmente o diretorio Json é:',diretoria_json,'\n')
    print('O que deseja alterar?:\n1)Alterar diretorio CSV\n2)Alterar diretorio Json\n')
    escolha_diretoria=int(input('\nEscolha a opccao desejada: '))
    if (escolha_diretoria==1):
        print('Actualmente o diretorio CSV é:',diretoria_csv,'\n')
        nova_diretoria_csv=input('Escreva a nova diretoria: ')
        print('\n')
        diretoria_csv=nova_diretoria_csv
        print('A diretoria foi alterada,actualmente o diretorio CSV é:',diretoria_csv )
    if (escolha_diretoria==2):
        print('Actualmente o diretorio Json é:',diretoria_json,'\n')
        nova_diretoria_json=input('Escreva a nova diretoria: ')
        print('\n')
        diretoria_json=nova_diretoria_json
        print('A diretoria foi alterada,actualmente o diretorio CSV é:',diretoria_json )        
    
    
programa_duarte()



